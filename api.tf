variable "region" {
  default     = "us-west-1"
  description = "The AWS region to store S3 data"
}

variable "project" {
  description = "The DOMAIN name of the project"
}

variable "stage" {
  default     = "development"
  description = "The stage of product development"
}

variable "key" {
  description = "Name of the state file to be stored"
}

output "URL" {
  description = "URL of S3 bucket"
  value       = aws_s3_bucket.state.website_endpoint
}