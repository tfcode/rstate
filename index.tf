provider "aws" {
  region = var.region
}


resource "aws_s3_bucket" "state" {
  bucket = "${var.project}-lowerentropy-state-${var.stage}"
  acl    = "authenticated-read"

  versioning {
    enabled = true
  }

  lifecycle {
    #   delete_after_create = true
  }
}